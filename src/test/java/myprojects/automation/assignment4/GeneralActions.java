package myprojects.automation.assignment4;


import myprojects.automation.assignment4.model.ProductData;
import myprojects.automation.assignment4.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;

    //login page
    private final By loginInput = By.id("email");
    private final By passwdInput = By.id("passwd");
    private final By submitButton = By.name("submitLogin");

    //menu items
    private final By subtabCatalog = By.xpath("//li[@data-submenu='9']");
    private final By subtabProducts = By.xpath("//li[@data-submenu='10']");

    private final By spinnerSpan = By.id("ajax_running");

    //products page
    private final By newProductButton = By.id("page-header-desc-configuration-add");
    private final By filterInput = By.name("filter_column_name");
    private final By filterButton = By.name("products_filter_submit");
    private final String productSelector = "//form[@id='product_catalog_list']//a[text()='%s']";

    //new product page
    private final By productNameInput = By.id("form_step1_name_1");
    private final By productAmountInput = By.id("form_step1_qty_0_shortcut");
    private final By productPriceInput = By.id("form_step1_price_ttc_shortcut");
    private final By productActiveButton = By.className("switch-input");
    private final By saveProductButton = By.cssSelector(".btn-primary.js-btn-save");
    private final By previewButton = By.id("product_form_preview_btn");
    private final By popupDiv = By.className("growl-notice");

    //shop main page
    private final By allLink = By.className("all-product-link");

    //product list shop page
    private final By pagesLink = By.xpath("//a[contains(@href, '?page=')][@rel='nofollow']");
    private final By productLink = By.xpath("//h1[contains(@class, 'product-title')]/a");

    //product shop page
    private final By detailsTab = By.xpath("//a[@href='#product-details']");
    private final By nameEl = By.cssSelector("ol > li:last-child");
    private final By priceEl = By.xpath("//span[@itemprop='price']");
    private final By amountEl = By.xpath("//div[@class='product-quantities']/span");


    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        driver.get(Properties.getBaseAdminUrl());

        wait.until(ExpectedConditions.visibilityOfElementLocated(loginInput));

        driver.findElement(loginInput).sendKeys(login);
        driver.findElement(passwdInput).sendKeys(password);
        driver.findElement(submitButton).click();

        waitForContentLoad();
    }

    /**
     * Waits until page loader disappears from the page
     */
    public void waitForContentLoad() { waitForContentLoad(true);}
    public void waitForContentLoad(boolean waitAjax) {
        Function<WebDriver, Boolean> isPageReady = d ->
                ((JavascriptExecutor) d).executeScript("return document.readyState").equals("complete");

        if(!isPageReady.apply(driver)) {
            wait.until(isPageReady);
            if(waitAjax)
                wait.until(ExpectedConditions.visibilityOfElementLocated(spinnerSpan));
        }
        if(waitAjax)
            wait.until(ExpectedConditions.invisibilityOfElementLocated(spinnerSpan));
    }

    public void goToProductList() {
        new Actions(driver)
                .moveToElement(driver.findElement(subtabCatalog))
                .perform();
        wait.until(ExpectedConditions.elementToBeClickable(subtabProducts));
        driver.findElement(subtabProducts).click();
        waitForContentLoad(false);
    }

    public String createProduct(ProductData product) {
        goToProductList();
        driver.findElement(newProductButton).click();

        waitForContentLoad(false);

        driver.findElement(productNameInput).sendKeys(product.getName());
        driver.findElement(productAmountInput).sendKeys(product.getQty().toString());
        driver.findElement(productPriceInput).sendKeys(product.getPrice());
        driver.findElement(productActiveButton).click();
        //driver.findElement(saveProductButton).click();

        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(popupDiv));

        return driver.findElement(previewButton).getAttribute("data-redirect");
    }

    public ProductData assembleProduct(String url) {
        driver.get(url);
        waitForContentLoad(false);

        driver.findElement(detailsTab).click();

        String name   = driver.findElement(nameEl).getText();
        String price  = driver.findElement(priceEl).getAttribute("content");
        String amount = driver.findElement(amountEl).getText().split("\\s+")[0];

        return new ProductData(name, Integer.parseInt(amount), Float.parseFloat(price));
    }

    public List<ProductData> findProducts(String name) {
        driver.get(Properties.getBaseUrl());
        waitForContentLoad(false);

        clickJS(driver.findElement(allLink));
        waitForContentLoad(false);

        List<String> pages = driver.findElements(pagesLink).stream()
                .map(e -> e.getAttribute("href"))
                .collect(Collectors.toList());

        ArrayList<String> productPages = new ArrayList<>();

        //TODO: Не перезагружать первую страницу
        for(String url : pages) {
            driver.get(url);
            waitForContentLoad(false);

            for(WebElement e : driver.findElements(productLink)) {
                if(e.getText().trim().equals(name))
                    productPages.add(e.getAttribute("href"));
            }
        }

        return productPages.stream()
                .map(this::assembleProduct)
                .collect(Collectors.toList());
    }

    private void clickJS(WebElement e) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", e);
    }
}

package myprojects.automation.assignment4.tests;

import myprojects.automation.assignment4.BaseTest;
import myprojects.automation.assignment4.model.ProductData;
import myprojects.automation.assignment4.utils.logging.CustomReporter;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.util.List;

public class CreateProductTest extends BaseTest {
    private ProductData newProduct;

    @DataProvider(name = "credentials")
    public Object[][] credentials() {
        return new Object[][] {
            {"webinar.test@gmail.com", "Xcg7299bnSmMuRLp9ITw"}};
    }

    @DataProvider(name = "products")
    public Object[][] products() throws ParseException {
        return new Object[][] {{ ProductData.generate() }};
    }

    @Test(dataProvider = "credentials")
    public void login(String login, String password){
        actions.login(login, password);
    }

    @Test(dependsOnMethods = "login", dataProvider = "products")
    public void createNewProduct(ProductData p) {

        CustomReporter.logAction("Login successfully");

        actions.createProduct(p);
        CustomReporter.logAction(String.format("%s created", p));
        newProduct = p;
    }

    @Test(dependsOnMethods = "createNewProduct")
    public void checkProduct() {
        List<ProductData> ps = actions.findProducts(newProduct.getName());

        Assert.assertTrue(ps.size() > 0, "No products found");
        Assert.assertTrue(ps.stream().anyMatch(newProduct::equals), "No matched products found");
    }
}

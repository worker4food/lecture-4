package myprojects.automation.assignment4.model;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.Random;
import com.github.javafaker.Faker;
import com.github.javafaker.Commerce;

/**
 * Hold Product information that is used among tests.
 */
public class ProductData {
    private String name;
    private int qty;
    private float price;

    public ProductData(String name, int qty, float price) {
        this.name = name;
        this.qty = qty;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public Integer getQty() {
        return qty;
    }

    public String getPrice() {
        DecimalFormatSymbols separators = new DecimalFormatSymbols();
        separators.setDecimalSeparator(',');
        return new DecimalFormat("#0.00", separators).format(price);
    }

    /**
     * @return New Product object with random name, quantity and price values.
     */
    public static ProductData generate() throws ParseException {
        Random random = new Random();
        Locale ru = new Locale("ru", "ru");
        NumberFormat fmt = NumberFormat.getInstance(ru);
        Commerce gen = new Faker(ru).commerce();

        return new ProductData(
                gen.productName(),
                random.nextInt(100) + 1,
                Float.parseFloat(gen.price().replace(",", ".")));
    }

    @Override
    public boolean equals(Object ob) {
        if (ob == null) return false;
        if (ob.getClass() != getClass()) return false;

        ProductData other = (ProductData)ob;

        return name.equals(other.getName())
                && qty == other.getQty()
                && getPrice().equals(other.getPrice());
    }

    @Override
    public String toString() {
        return String.format("%s {'%s', %.2f, %d}", getClass().getSimpleName(), name, price, qty);
    }
}
